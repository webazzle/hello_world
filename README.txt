Workfront Hello World Module
================================================================================

- Installing: Clone into hello_world directory in modules/custom directory

- Enabling: Use standard Drupal UI or drush

- The initial Hello World module creates a database table upon install of the module.

- It adds a Primary ID as a serialized field along with examples of int, varchar, blob and bigint.

- When the module is disabled, this table will be removed. hook_schema provides this functionality.

- The module also requires at least Drupal 8.5 based on a dependency in the info.yml file

- There is a library that is attached when the homepage is loaded. This is based on the hook_preprocess_page
	function in the .module file.
	
- You can verify the JS file is loaded by a console.log message

Superman Block
================================================================================

- There is a custom block that you can place usign standard Drupal block placement

- There are two custom fields along with the use of the built in Title field

- There are two template files. One alters the default block template so we can re-write the Title field, the second one outputs
	custom markup in the block
	
- There is some simple CSS that comes from another library, attached during the build()

Superman Block (Character blocks)
================================================================================

- Using a plugin derivative, there are three extra blocks that are available to place

- I used a new block plugin though I could technically have ran them all off the Superman plugin, but I wanted to do two different implementations to see
  how it worked side by side.

- These are using the same library/template for output as the singular Superman block

- This was my first time implementing this type of plugin so I imagine a lot more can be learned, especially depending on the specific requirement

Superman Custom Form
================================================================================

- There is a custom form that lives on the URL /my-custom-form

- When you submit the form, the form values will be output as you are redirected to the homepage.

Superman Custom Route
================================================================================

- There is a custom page at /my-custom-route/{your text}

- Text that you place on that url will show up in the page using a template file for wrapping markup

Marvel Custom Theme
================================================================================

- The child theme 'Marvel' is created in another repository https://bitbucket.org/webazzle/marvel/src/master/

- There is minimal edits showing the changes that can be made to a content type template file

- Small theming edits have been applied within a css file compiled from simple a simple SASS file to demonstrate implementation

Superman Custom Paragraph
================================================================================

- The hello_world module contains dependencies and configuration for a custom paragraph that is enabled when the module is installed

- There are three fields along with a entity_view_alter hook that applies class(es) to the paragraph via a text field

- The template file is contained within the custom module as to allow base theming to follow regardless of the global theme on the site
