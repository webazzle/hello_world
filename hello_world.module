<?php

/**
 * @file
 * Provides custom functionality for the Workfront Hello World module
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
	
/**
 * Implements hook_preprocess_page().
 */
function hello_world_preprocess_page(&$variables) {
	// Checks if our current route is the homepage, then adds a library with JS and CSS
	if (\Drupal::routeMatch()->getRouteName() == 'view.frontpage.page_1') {
		$variables['#attached']['library'][] = 'hello_world/superheros';
	}
}

/**
 * Implements hook_form_alter().
 *
 **/
function hello_world_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
	// We're going to remove the checkbox to give the user a choice to display to title
	// The title will be output anyways
	if(isset($form['id']['#default_value'])) {
	  if($form['id']['#default_value'] == 'supermanblock') {
		  $form['settings']['label_display']['#type'] = 'hidden';
	  }
  }
}

/**
 * Implements hook_ENTITY_TYPE_view_alter().
 */
function hello_world_paragraph_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {

	// We have a text field where you can put classes and apply it to the paragraph
  if(!empty($entity->field_css_class)) {
    if($build['#view_mode'] == 'default') {      
      if(!empty($entity->field_css_class->getValue())) {
        $classes = $entity->field_css_class->getValue()[0]['value'];
        $classes_array = explode(',', $classes);
        foreach ($classes_array as $a_class) {
          $a_class = trim($a_class);
          if (!empty($a_class)) {
            $build['#attributes']['class'][] = $a_class;
          }
        }
      }
    }
  }
		
}

/**
 * Implements hook_theme().
 *
 * To add the template definition.
 **/
function hello_world_theme($existing, $type, $theme, $path) {
	// We are returning the base block template to remove the default block title
	// We also have a custom output for the fields in a seperate tempalte file
  return [
    'superman_block' => [
      'template' => 'superman--block',
      'variables' => [
        'image' => NULL,
        'title' => NULL,
        'description' => NULL,
      ],
    ],
    'block__hello_world' => [
	    'template' => 'block--hello-world-superman',
	    'base hook' => 'block'
    ],
    'superman_route' => [
      'template' => 'superman--route',
      'variables' => [
        'text' => NULL,
      ],
    ],    
    'paragraph__workfront_heros' => [
	    'template' => 'paragraph--workfront-heros',
	    'base hook' => 'paragraph'
    ],
  ];
}
