<?php
/**
 * @file
 * Contains \Drupal\hello_world\Form\myCustomForm.
 */

namespace Drupal\hello_world\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Render\Element\Fieldset;
use Symfony\Component\HttpFoundation\RedirectResponse;

class myCustomForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'my_custom_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

	  // Fieldset
	  $form['my_custom_wrapper'] = array(
		  '#type' => 'fieldset',
		  '#title' => t('My Custom Fieldset'),
	  );

	  // Text field
	  $form['my_custom_wrapper']['my_custom_text'] = array(
		  '#type' => 'textfield',
		  '#title' => t('Textfield'),
		  '#description' => t('Enter your text.'),
	  );

	  // Image Field
	  $form['my_custom_wrapper']['my_custom_image'] = array(
	    '#type' => 'managed_file',
	    '#upload_location' => 'public://',
	    '#title' => t('Image'),
	    '#upload_validators' => [
	        'file_validate_extensions' => ['jpg jpeg png gif']
	    ],
	    '#description' => t('Upload your image.'),
	  );	

	  // Select field
	  $form['my_custom_wrapper']['my_custom_select'] = array(
		  '#type' => 'select',
		  '#title' => t('Select'),
		  '#description' => t('Select an option from the select list.'),
		  '#options' => array(
			  0 => t('Option 1'),
			  1 => t('Option 2'),
			  2 => t('Option 3'),
		  ),
	  );

		// Radio field
		$form['my_custom_wrapper']['my_custom_radios'] = array(
			'#type' => 'radios',
			'#title' => t('Radios'),
			'#description' => t('Select a radio option from the list.'),
		  '#options' => array(
			  0 => t('Radio Option 1'),
			  1 => t('Radio Option 2'),
			  2 => t('Radio Option 3'),
		  ),			
		);

		// Email
		$form['my_custom_wrapper']['my_custom_email'] = array(
			'#type' => 'email',
			'#title' => t('E-mail'),
			'#description' => t('Please enter your e-mail.'),
		);

		// Telephone
		$form['my_custom_wrapper']['my_custom_phone'] = array(
			'#type' => 'tel',
			'#title' => t('Telephone'),
			'#description' => t('Please enter your telephone number.'),
		);

	  // Submit button
    $form['my_custom_wrapper']['actions']['submit'] = array(
	    	'#type' => 'submit',
	    	'#value' => $this->t('Submit'),
	  );

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	  
	  // Creating a manual array so we can iterate and output the submission values
	  $form_fields = array(
		  'Textfield' => 'my_custom_text',
		  'Select' => 'my_custom_select',
		  'Radios' => 'my_custom_radios',
		  'E-Mail' => 'my_custom_email',
		  'Phone' => 'my_custom_phone'
	  );
	  
	  // Create an empty array
	  // Creating multiple arrays is generally unnecessary, but for demonstration purposes, we can use
	  // the submission values in a myriad of ways.
	  $results = array();
	  
	  // Get all the submitted values from the form
	  $submitted_values = $form_state->getValues();
	  
	  // From each of the basic fields, populate the array
	  foreach($form_fields as $label => $submitted_item) {
		  $results[$submitted_item] = $submitted_values[$submitted_item];
	  }
	  
	  // Outputting a basic message
	  drupal_set_message($this->t('Your form has been submitted.'));
	  // Outputting the results in a key|value or stating that there is no value.
	  foreach($results as $label => $result) {
		  if(strlen(trim($result)) > 0) {
				drupal_set_message($this->t('@label|@result',array('@label' => $label, '@result' => $result)));
				
		  } else {
			  drupal_set_message($this->t('@label has no data',array('@label' => $label)));
		  }
	  }
	  
	  // Since the image field has more information, we will process that seperately.
	  // The base info that comes from this is the FID. This can be passed on for processing if needed.
	  $image_info =  $form_state->getValues()['my_custom_image'];
	  if(!empty($form_state->getValues()['my_custom_image'])) {
		  $file_info = $this->getFileInfo($form_state->getValues()['my_custom_image'][0]);
		  drupal_set_message($this->t('The file you uploaded is named @filename',array('@filename' => $file_info)));
	  } else {
		  drupal_set_message($this->t('You did not upload a file'));
	  }
	  
	  $response = new RedirectResponse(\Drupal::url('<front>'));
	  $response->send();
    
  }

  /**
   * {@inheritdoc}
   */
  public function getFileInfo($fid) {
	  // Load the file
	  $file = \Drupal\file\Entity\File::load($fid);
	  return $file->getFilename();
  }
  
  
}