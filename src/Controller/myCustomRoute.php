<?php

namespace Drupal\hello_world\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

class myCustomRoute extends ControllerBase {
  /**
   * Display the markup.
   *
   * @return array
   */
  public function content(Request $request, $text = NULL) {
    
    $output['#text'] = $text;
    $output['#theme'] = 'superman_route';
    
    // Using the CSS Library that we used for the block.
    $output['#attached'] = array(
			'library' => array(
				'hello_world/supermanblock',
			),
    );
    
    return $output;
  }
}
