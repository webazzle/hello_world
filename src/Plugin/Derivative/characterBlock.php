<?php

/**
 * @file
 * Contains \Drupal\hello_world\Plugin\Derivative\characterBlock.
 */

namespace Drupal\hello_world\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for hello world character blocks.
 *
 * @see \Drupal\hello_world\Plugin\Block\characterBlock
 */
class characterBlock extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
	  $character_blocks = array(
	      'character_batman' => t('Character: Batman Block'),
	      'character_flash' => t('Character: Flash Block'),
	      'character_wonder_woman' => t('Character: Wonder Woman'),
	  );
	
	  foreach ($character_blocks as $block_id => $block_label) {
	      $this->derivatives[$block_id] = $base_plugin_definition;
	      $this->derivatives[$block_id]['admin_label'] = 
	          t('@name', array('@name' => $block_label));
	  }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }
}
