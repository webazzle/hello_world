<?php

namespace Drupal\hello_world\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'characterBlock' block plugin.
 *
 * @Block(
 *   id = "character_block",
 *   admin_label = @Translation("Character block"),
 *   deriver = "Drupal\hello_world\Plugin\Derivative\characterBlock"
 * )
 */

class characterBlock extends BlockBase {

	/**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {

	  $form['character_block_logo'] = array(
	    '#type' => 'managed_file',
	    '#upload_location' => 'public://',
	    '#title' => t('Image'),
	    '#upload_validators' => [
	        'file_validate_extensions' => ['jpg jpeg png gif']
	    ],
	    '#default_value' => isset($this->configuration['character_block_logo']) ? $this->configuration['character_block_logo'] : '',
	    '#description' => t('The image to display'),
	    '#weight' => 5,
	  );
	  
    $form['character_block_description'] = array(
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Character Image Description'),
      '#description' => $this->t('Enter some information about the character'),
      '#default_value' => isset($this->configuration['character_block_description']) ? $this->configuration['character_block_description'] : '',
      '#weight' => 7,
    );
        
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['character_block_logo'] = $form_state->getValue('character_block_logo');
    $this->configuration['character_block_description'] = $form_state->getValue('character_block_description')['value'];
    $this->configuration['character_block_title'] = $form_state->getValue('character_block_title')['value'];
		
		// Set the image file as permanent, otherwise Drupal gets rid of it
		if($this->configuration['character_block_logo'][0]) {
			$fid = $this->configuration['character_block_logo'][0];
	    $file = \Drupal\file\Entity\File::load($fid);
	    $file->setPermanent();
	    $file->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {	 
    
    $output = array();
    
    // Check for a image file
    if($this->configuration['character_block_logo'][0]) {
	    // Load the File ID
	    $fid = $this->configuration['character_block_logo'][0];
	    $file = \Drupal\file\Entity\File::load($fid);
			
			// Set variables to build the render array
	    $variables = array(
			  'style_name' => 'large',
			  'uri' => $file->getFileUri(),
			);
			
			// Load the image to check validity and get some dimensions
			$image = \Drupal::service('image.factory')->get($file->getFileUri());
			if ($image->isValid()) {
			  $variables['width'] = $image->getWidth();
			  $variables['height'] = $image->getHeight();
			}
			else {
			  $variables['width'] = $variables['height'] = NULL;
			}			
	    
	    // Add the image
	    $output['#image'] = array(
				'#theme' => 'image_style',
				'#width' => $variables['width'],
				'#height' => $variables['height'],
				'#style_name' => $variables['style_name'],
				'#uri' => $variables['uri'],
	    );
    } else {
	    // Output null so we can properly display variants
	    $output['#image'] = NULL;
    }
    
    // We get the title
    $output['#title'] = $this->configuration['label'];;
    
		// We get a description as well
		if($this->configuration['character_block_description']) {
			$output['#description'] = $this->configuration['character_block_description'];
		} else {
			$output['#description'] = NULL;
		}
		    
    $output['#theme'] = 'superman_block';
    $output['#attached'] = array(
			'library' => array(
				'hello_world/supermanblock',
			),
    );
    	return $output;

    }
}
